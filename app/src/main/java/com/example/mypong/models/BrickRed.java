package com.example.mypong.models;

import android.graphics.Color;

public class BrickRed extends BrickModel{

    public BrickRed(float coordX, float coordY) {
        super(coordX, coordY);
    }

    private int vulnirability=1;

    @Override
    public  int getColor(){
        if(vulnirability==0){
            return Color.WHITE;
        }else{
            return  Color.RED;
        }
    }

    @Override
    public void setBroken(boolean broken) {
        if(broken && vulnirability>0) {
            vulnirability = 0;
        }else if(!broken){
            vulnirability= 1;
            super.setBroken(broken);
        }else{
            super.setBroken(broken);
        }
    }
}
