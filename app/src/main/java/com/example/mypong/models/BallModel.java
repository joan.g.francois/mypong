package com.example.mypong.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

public class BallModel extends  ObjectCanvas{

    private float radius = 50; // Ball's radius
    private float speedX;  // Ball's speed (x,y)
    private float speedY;

    public BallModel() {
        setColor(Color.YELLOW);
        speedX = 20;
        speedY = 20;
    }

    public float getCoordLeft(){
        return getCoordX() - radius;
    }

    public float getCoordTop(){
        return getCoordY() - radius;
    }

    public float getCoordRight(){
        return getCoordX() + radius;
    }

    public float getCoordBottom(){
        return getCoordY() + radius;
    }

    public void updatePosition(){
        incrCoordX(speedX);
        incrCoordY(speedY);
    }

    public void changeDirectionBottom(){
        speedY = -speedY;
        setCoordY(getCoordBottom());
    }

    public void changeDirectionTop(){
        speedY = -speedY;
        setCoordY(getCoordTop());
    }

    public void changeDirectionLeft(){
        speedX= -speedX;
        setCoordX(getCoordLeft());
    }

    public void changeDirectionRight(){
        speedX= -speedX;
        setCoordX(getCoordRight());
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        super.draw(canvas, paint);
        RectF ballBounds = new RectF();
        ballBounds.set(getCoordLeft(), getCoordTop(), getCoordRight(), getCoordBottom());
        canvas.drawOval(ballBounds, paint);
    }
}
