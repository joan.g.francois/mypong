package com.example.mypong.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class PlayerModel extends ObjectCanvas {

    private int speed= 100;
    private int width= 300;
    private int height= 100;

    public PlayerModel() {
        setColor(Color.WHITE);
    }

    public void moveLeft(){
        setCoordX(getCoordX() - speed);
    }

    public void moveRight(){
        setCoordX(getCoordX() + speed);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        super.draw(canvas, paint);
        canvas.drawRect(getCoordX(), getCoordY(), getCoordX()+getWidth(), getCoordY()+getHeight(), paint);
    }

    public boolean isCollision(BallModel ball){
        return ball.getCoordBottom()>= getCoordY() &&
                ball.getCoordX()>= getCoordX() &&
                ball.getCoordX() <= getCoordX() + width;
    }
}
