package com.example.mypong.models;

public interface IGameObserver {

    public void addPoint(int point);
    public void removeLife();

}
