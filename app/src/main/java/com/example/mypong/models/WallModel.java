package com.example.mypong.models;

import java.util.ArrayList;

public class WallModel {

    protected int nbrLine= 3;
    protected ArrayList<BrickModel> bricks;

    public WallModel(){
        int startX= 20, startY=20;
        bricks= new ArrayList<BrickModel>();
        for(int i=0; i<5; i++){
            for(int level=0; level<nbrLine; level++){
                    bricks.add(new BrickModel(startX + 205 * i, startY + 105 * level));
            }
        }
    }

    public ArrayList<BrickModel> getBricks() {
        return bricks;
    }

    public boolean isBroken(){
        for(BrickModel brick: bricks){
            if(!brick.isBroken()){
                return false;
            }
        }
        return true;
    }

}
