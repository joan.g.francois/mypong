package com.example.mypong.models;

import java.util.ArrayList;

public class WallModel3 extends  WallModel{

    public WallModel3(){
        nbrLine = 6;
        bricks= new ArrayList<BrickModel>();
        int startX= 20, startY=20;
        for(int i=0; i<5; i++){
            for(int level=0; level<nbrLine; level++){
                if(i%2==0 && level%2==0){
                    bricks.add(new BrickModel(startX + 205 * i, startY + 105 * level));
                }

            }
        }
    }
}
