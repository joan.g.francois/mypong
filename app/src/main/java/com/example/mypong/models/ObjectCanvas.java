package com.example.mypong.models;

import android.graphics.Canvas;
import android.graphics.Paint;

public class ObjectCanvas {

    private float coordX;
    private float coordY;
    private int color;

    public void incrCoordX(float val){
        coordX+= val;
    }
    public void incrCoordY(float val){
        coordY+= val;
    }

    public float getCoordX() {
        return coordX;
    }

    public void setCoordX(float coordX) {
        this.coordX = coordX;
    }

    public float getCoordY() {
        return coordY;
    }

    public void setCoordY(float coordY) {
        this.coordY = coordY;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void draw(Canvas canvas, Paint paint){
        paint.setColor(color);
    }
}
