package com.example.mypong.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class BrickModel extends ObjectCanvas{

    private float width = 200;
    private float height = 100;
    private boolean isBroken = false;

    public BrickModel(float coordX, float coordY) {
        setCoordX(coordX);
        setCoordY(coordY);
    }

    public int getColor() {
        return Color.WHITE;
    }

    public boolean isColision(BallModel ball) {
        return  ball.getCoordX() >= getCoordX() &&
                ball.getCoordX() <= getCoordX() + width &&
                ball.getCoordY() >= getCoordY() &&
                ball.getCoordY() <= getCoordY()+height;
    }

    public boolean isBroken() {
        return isBroken;
    }

    public void setBroken(boolean broken) {
        isBroken = broken;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        paint.setColor(getColor());
        canvas.drawRect(getCoordX(), getCoordY(), getCoordX()+getWidth(), getCoordY() +getHeight(), paint);
    }
}