package com.example.mypong.models;

import java.util.ArrayList;

public class WallModel6 extends  WallModel{

    public WallModel6(){
        nbrLine = 3;
        bricks= new ArrayList<BrickModel>();
        int startX= 20, startY=20;
        for(int i=0; i<5; i++){
            for(int level=0; level<nbrLine; level++){
                bricks.add(new BrickRed(startX + 205 * i, startY + 105 * level));
            }
        }
    }
}
