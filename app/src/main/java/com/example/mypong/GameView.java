package com.example.mypong;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;

import com.example.mypong.models.BallModel;
import com.example.mypong.models.BrickModel;
import com.example.mypong.models.BrickRed;
import com.example.mypong.models.IGameObserver;
import com.example.mypong.models.PlayerModel;
import com.example.mypong.models.WallModel;
import com.example.mypong.models.WallModel2;
import com.example.mypong.models.WallModel3;
import com.example.mypong.models.WallModel4;
import com.example.mypong.models.WallModel5;
import com.example.mypong.models.WallModel6;

import java.util.ArrayList;

public class GameView extends View {

    private int xMin = 0;          // This view's bounds
    private int xMax;
    private int yMin = 0;
    private int yMax;

    private int currentLevel=0;
    private BallModel ball;
    private ArrayList<WallModel> walls;
    private PlayerModel player;
    private Paint paint;           // The paint (e.g. style, color) used for drawing

    private IGameObserver observer;

    // Constructor
    public GameView(Context context) {
        super(context);
        player= new PlayerModel();
        ball = new BallModel();
        walls= new ArrayList<WallModel>();
        walls.add(new WallModel());
        walls.add(new WallModel2());
        walls.add(new WallModel3());
        walls.add(new WallModel4());
        walls.add(new WallModel5());
        walls.add(new WallModel6());
        paint = new Paint();
    }

    public void setObserver(IGameObserver observer) {
        this.observer = observer;
    }

    public void restartGame(){
        ball.setCoordX(xMax/2);
        ball.setCoordY(yMax/2);

        for(BrickModel brick: walls.get(currentLevel).getBricks()){
            brick.setBroken(false);
        }
    }

    // Called back to draw the view. Also called by invalidate().
    @Override
    protected void onDraw(Canvas canvas) {
        for(BrickModel brick: walls.get(currentLevel).getBricks()){
            if(!brick.isBroken()){
                brick.draw(canvas, paint);
            }
        }

        // Draw the ball
        ball.draw(canvas, paint);

        // Draw the player barre
        player.draw(canvas, paint);

        // Update the position of the ball, including collision detection and reaction.
        update();

        // Delay
        try {
            Thread.sleep(30);
        } catch (InterruptedException e) { }

        invalidate();  // Force a re-draw
    }

    // Detect collision and update the position of the ball.
    private void update() {
        // Get new (x,y) position
        ball.updatePosition();

        for(BrickModel brick: walls.get(currentLevel).getBricks()){
            if(!brick.isBroken() && brick.isColision(ball)){
                brick.setBroken(true);
                ball.changeDirectionBottom();
                observer.addPoint(10);
            }
        }

        // Detect collision and react
        if (ball.getCoordRight() > xMax) {
            ball.changeDirectionLeft();
        } else if (ball.getCoordLeft() < xMin) {
            ball.changeDirectionRight();
        } else if (ball.getCoordTop() < yMin) {
            ball.changeDirectionBottom();
        }else if(player.isCollision(ball)){
            ball.changeDirectionTop();
        }else if(ball.getCoordTop() > yMax){
            observer.removeLife();
            restartGame();
        }else if(walls.get(currentLevel).isBroken()&&
                currentLevel<walls.size()-1){
            currentLevel++;
        }
    }

    // Called back when the view is first created or its size changes.
    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        // Set the movement bounds for the ball
        xMax = w-1;
        yMax = h-1;

        player.setCoordX(xMax/3);
        player.setCoordY(yMax-150);

        ball.setCoordX(xMax/2);
        ball.setCoordY(yMax/2);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        if (eventX < player.getCoordX()) {
            player.moveLeft();
        }else if(eventX >  player.getCoordX() + player.getWidth()){
            player.moveRight();
        }
        return true;
    }
}