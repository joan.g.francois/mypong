package com.example.mypong;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mypong.models.IGameObserver;

public class MainActivity extends AppCompatActivity implements IGameObserver {

    FrameLayout frameLayout;
    TextView textViewScore;
    ImageView imageLife, imageLife2, imageLife3;

    int score=0;
    int life= 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);
        imageLife  = (ImageView) findViewById(R.id.imageLife1);
        imageLife2 = (ImageView) findViewById(R.id.imageLife2);
        imageLife3 = (ImageView) findViewById(R.id.imageLife3);

        textViewScore = (TextView) findViewById(R.id.textViewScore);
        frameLayout = (FrameLayout) findViewById(R.id.frameGame);
        GameView bouncingBallView = new GameView(this);
        bouncingBallView.setObserver(this);
        frameLayout.addView(bouncingBallView);
    }

    @Override
    public void addPoint(int point) {
        score+= point;
        textViewScore.setText("Score : "+score);
    }

    @Override
    public void removeLife() {
        life= life -1;
        switch (life){
            case 2:
                imageLife.setVisibility(View.INVISIBLE);
                break;
            case 1:
                imageLife2.setVisibility(View.INVISIBLE);
                break;
            default:
                imageLife3.setVisibility(View.INVISIBLE);
                break;
        }
        if(life<0){
            frameLayout.removeAllViews();
            TextView textView= new TextView(this);
            textView.setText("GAME OVER");
            textView.setTextColor(Color.YELLOW);
            textView.setTextSize(32);
            textView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
            textView.setGravity(Gravity.CENTER);
            frameLayout.addView(textView);
        }
    }
}